// jshint esversion:6
const Jasmine = require('jasmine');
const jasmine = new Jasmine();
const TSConsoleReporter = require('jasmine-ts-console-reporter');

let jasmineReporter;
jasmineReporter = new TSConsoleReporter();
jasmine.reporter = jasmineReporter;
jasmine.addReporter(jasmineReporter);

jasmine.loadConfig({
  spec_files: [
    'dist/**/*[sS]pec.js'
  ],
  helpers: [
    'helpers/**/*.js'
  ]
});

jasmine.execute();
