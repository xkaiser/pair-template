import * as Main from './main'
describe('Main', () => {

    describe("doSomething", () => {
        let unit: any;
        beforeEach(() => {
            unit = Main.default;
        })

        describe('when n = 1', () => {
            it("should return", () => {
                let val = unit.doSomething(1)
                expect(val).toBe(1)
            })
        })

        describe('n = 2', () => {
            it('return 3', () => {
                let val = unit.doSomething(2)
                expect(val).toBe(3)
            })
        })

        describe('n = 3', () => {
            it('return roku!', () => {
                let val = unit.doSomething(3)
                expect(val).toBe(6)
            })
        })

        describe('n = 4', () => {
            it('return jyu!', () => {
                let val = unit.doSomething(4)
                expect(val).toBe(10)
            })
        })

        describe('n = 10', () => {
            it('return jyu!', () => {
                let val = unit.doSomething(10)
                expect(val).toBe(55)
            })
        })

        describe('n = 100', () => {
            it('return go hyaku go jyu!', () => {
                let val = unit.doSomething(100);
                expect(val).toBe(5050);
            })
        })

        describe('n = 1000000000000', () => {
            it('return go hyaku go jyu!', () => {
                let val = unit.doSomething(1000000000000);
                expect(val).toBe(5.000000000005e+23);
            })
        })

    })
})