/***
 *   Number of cirlces in a triangle!
 * 
 *    o     n = 1     f(n) = 1 = 1
 * 
 *    o
 *   o o    n = 2     f(n) = 1 + 2 = 3
 * 
 *    o
 *   o o    n = 3    f(n) = 3 + 3 = 6
 *  o o o
 * 
 *   1 / 2 * base * height = area triangle
 *            9 + 3 / 2 = 12 / 2 = 6
 *            (n^2 + n) / 2 = 6
 * n^2 = n * n
 * n^3 = n * n * n
 * 
 *    o
 *   o o    n = 4    f(n) = 6 + 4 = 10
 *  o o o
 * o o o o
 * 
 * big circles! n = 100   f(n) = 5050
 * 
 * Big O notation?
 * Old solution = O(n*n)
 * New solution = O(1)
 ***/


function doSomething(someNumber: number): number {
    let someValue = (someNumber * someNumber + someNumber) / 2
    return someValue;
}

export default {
    doSomething
}